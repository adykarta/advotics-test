import React, { Component }from 'react';

const Auth = {
	
	isAuthenticated: true,
    token: '',
    
	setAuth() {
		this.isAuthenticated = true
	},

	signout() {
		this.isAuthenticated = false
		this.setAuthToken('')
		this.setRoles()
	},

	getAuth() {
		return this.isAuthenticated
	},

	setAuthToken(tokenStr) {
		this.token = tokenStr
	},

	getAuthToken() {
		return this.token
	},


}

export default Auth;