import React, { Fragment } from 'react';
import { makeStyles} from '@material-ui/core/styles';
import { Box, Grid, Typography, IconButton } from '@material-ui/core';
import CalendarLogo from '../assets/calendar.png';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ClearIcon from '@material-ui/icons/Clear';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import 'date-fns';
import {isAfter} from 'date-fns';
import HelpLogo from '../assets/Help.png'
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import SalesTurnover from '../components/SalesTurnover';
import AveragePurchase from '../components/AveragePurchase';
import Card from '../components/Card';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
// import { DateRangePicker } from 'react-date-range';
import DateRangePicker from '../components/customizeDate/DateRangePicker/index'
import { addDays } from 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import Competitor from '../assets/competitor.png'
import Activia from '../assets/activia.jpg'
import Danone from '../assets/danone.jpeg'

const bestSelling = {
    head:{
        img:Activia,
        title:'[Nama Product]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },
    body:[{
        img:Danone,
        title:'[Nama Product A]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },
        {
        img:Danone,
        title:'[Nama Product B]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },
        {
        img:Danone,
        title:'[Nama Product C]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },        
        {
        img:Danone,
        title:'[Nama Product D]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },
    ]
}
const competitor = {
    head:{
        img:Competitor,
        title:'[Nama Product]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },
    body:[{
        img:Competitor,
        title:'[Nama Product A]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },
        {
        img:Competitor,
        title:'[Nama Product B]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },
        {
        img:Competitor,
        title:'[Nama Product C]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },        
        {
        img:Competitor,
        title:'[Nama Product D]',
        price:'RP XXX',
        terjual:'[jml terjual]',

        },
    ]
}

const styles = makeStyles(theme=>({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& > *': {
          margin: theme.spacing(1),
        },
      },
    header:{
        display:"flex",
        justifyContent:'space-between',
        alignItems:'center'
    },
    dropdown:{
        height:'48px',
        minWidth:'481px',
        backgroundColor:'white',
        boxShadow: '0px 2px 3px #00000029',
        borderRadius:"2px",
        display:'flex',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingLeft:'16px',
        paddingRight:'16px',

    },
    dropdownContent:{
        position:'absolute',
        backgroundColor:'white',
        boxShadow: '0px 2px 3px #00000029',
        minWidth:'1000px',
        height:'auto',
        paddingLeft:"16px", 
        paddingRight:'16px',
        paddingTop:'16px',
        right:0
       
        
    },
    dropdownContainer:{
        position:'relative'
    },
    icon:{
        color:'#8B8B8B',
        '&:hover':{
            cursor:'pointer'
        }
    }
    ,
    verticalLine:{
        height:"80%",
        width:'2px',
        backgroundColor:"#707070C4"
    },
    market:{
        backgroundColor:'#37B04C',
        height:'48px',
        width:'100%',
        marginTop:'31px',
        display:'flex',
        justifyContent:'space-between',
        alignItems:"center",
        padding:"0px 16px 0px 16px"
    },
    input:{
        fontWeight:'bold'
    },
   
}))
const Dashboard =()=>{
    const classes = styles()
   
    const [periodDate, setPeriodDate] = React.useState({
        startDate:'5 February 2020',
        endDate:'5 February 2020'
    })
    const [isOpen, setIsOpen]= React.useState({
        period:false,
    })
    const [state, setState] =React.useState([
        {
          startDate: new Date(),
          endDate: addDays(new Date(), 7),
          key: 'selection',
          color:'#37B04C'
        }
      ]);
    const handleOpen = (param)=>{
        setIsOpen(prev=>({...prev,[param]:true}))
    }
    const handleClose = (param) =>{
        setIsOpen(prev=>({...prev,[param]:false}))
    }
    const getMonth = (month)=>{
        if(month==='Jan'){
            return 'January'
        }
        else if(month==='Feb'){
            return 'February'
        }
        else if(month==='Mar'){
            return 'March'
        }
        else if(month==='Apr'){
            return 'April'
        }
        else if(month==='May'){
            return 'May'
        }
        else if(month==='Jun'){
            return 'June'
        }
        else if(month==='Jul'){
            return 'July'
        }
        else if(month==='Aug'){
            return 'August'
        }
        else if (month==='Sep'){
            return 'September'
        }
        else if(month==='Oct'){
            return 'October'
        }
        else if (month==='Nov'){
            return 'November'
        }
        else if(month==='Des'){
            return 'Desember'
        }
    }
    const parseDateToString = (date)=>{
        var dateString = date.toString();
        var dateSplit = dateString.split(' ');
        let month = getMonth(dateSplit[1]);
        return dateSplit[2]+' '+ month +' '+  dateSplit[3]
    }
    const handleDateChange = (date, param) => {
        var dateParsed = parseDateToString(date)
        setPeriodDate(prev=>({...prev,[param]:dateParsed}));
      };
    
  
    const handleApply = ()=>{
        const maxDate = new Date()
        const isOutsideMax = (maxDate && isAfter(state[0].endDate, maxDate));
        handleDateChange(state[0].startDate,'startDate')
        if(isOutsideMax){
           
            handleDateChange(maxDate,'endDate')
        }
        else{
           
            handleDateChange(state[0].endDate,'endDate')
        }
        handleClose('period')
     
    }
    return(
        <Fragment>
          <Grid container>
            {/* Header Section */}
            <Grid item xs={12}>
                <Box className={classes.header}>
                    <Typography style={{fontSize:'40px', color:'#707070C4'}}>Dashboard</Typography>
                    <div className={classes.dropdownContainer}>
                        <Box className={classes.dropdown}>
                            <Box style={{display:'flex', flexDirection:'row'}}>
                                <img src={CalendarLogo}></img>
                                <Typography style={{marginLeft:'12px', color:'#8B8B8B'}}>Period</Typography>
                                <Typography style={{marginLeft:'18px', color:'#6A6A6A'}}>{periodDate.startDate}&nbsp;-&nbsp;{periodDate.endDate}</Typography>
                            </Box>
                            <IconButton style={{marginLeft:'12px'}}>
                                <ExpandMoreIcon onClick={()=>handleOpen('period')}/>
                            </IconButton>
                           
                        </Box>
                        <Box className={classes.dropdownContent} style={isOpen.period ?{display:"block"}:{display:"none"}}>
                            <Box style={{display:'flex', flexDirection:'row', justifyContent:"space-between", marginBottom:'8px'}}>
                                <Box style={{display:'flex', flexDirection:'row'}}>
                                    <img src={CalendarLogo} style={{fontWeight:'bold'}}></img>
                                    <Typography style={{marginLeft:'12px', color:'#4D4F5C', fontWeight:'bold'}}>Period</Typography>

                                </Box>
                               
                                <ClearIcon onClick={()=>handleClose('period')} className={classes.icon}/>
                          
                            </Box>
                            <Grid container>
                              
                                <Grid item xs={12}>
                                    <DateRangePicker
                                       onChange={item => setState([item.selection])}
                                       showSelectionPreview={true}
                                       moveRangeOnFirstSelection={false}
                                       months={2}
                                       ranges={state}
                                        handleApply={handleApply}
                                    
                                       direction="horizontal"
                                    />
                                   
                                </Grid>
                            </Grid>
                        </Box>

                    </div>
                  
                </Box>
            </Grid>
            {/* Market Insight Section */}
            <Grid item xs={12}>
                <Box className={classes.market}>
                    <Typography style={{color:'white', fontWeight:'bold', fontSize:'20px'}}>MARKET INSIGHTS</Typography>
                    <Box style={{display:"flex", flexDirection:'row', alignItems:'center'}}>
                        <img src={HelpLogo} style={{width:"20px",height:'20px'}}/>
                        <Typography style={{color:"white", textDecoration:'underline', marginLeft:'4px'}}>Click here for help</Typography>
                      
                        <ExpandLessIcon style={{color:'white', marginLeft:'8px', '&:hover':{cursor:"pointer"}}}/>
                     
                    </Box>
                </Box>
                
            </Grid>
            {/* Sales Turnover Section */}
            <SalesTurnover/>
            {/* Content */}
            <Grid container>
                <Grid item xs={12} lg={6}>
                    <AveragePurchase/>
                </Grid>
                <Grid item xs={12} lg={3}>
                    <Card title={'BEST SELLING SKU'} content={bestSelling}/>
                </Grid>
                <Grid item xs={12} lg={3}>
                    <Card title={'TOP COMPETITOR SKU'} content={competitor}/>
                </Grid>
            </Grid>


          </Grid>
        </Fragment>
    )
} 
export default Dashboard