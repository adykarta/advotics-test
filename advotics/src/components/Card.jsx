import React, {Fragment} from 'react';
import {makeStyles, Box, Typography} from '@material-ui/core';
import MoreLogo from '../assets/more.svg';

const styles = makeStyles(theme=>({
    container:{
        width:'275px',
        height:'424px',
        backgroundColor:'white',
        boxShadow: '0px 2px 6px #0000000A',
        border: '0.5px solid #CACED5',
        padding:"0px 16px 0px 16px"

    },
    header:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        padding:'8px 0px 8px 0px',
        alignItems:'center'
    },
    logo:{
        width:"12px",
        height:"auto",
        '&:hover':{
            cursor:"pointer"
        }
    },
    content:{
        border: '1px solid #C5C5C59C',
        borderRadius: '4px',
        backgroundColor:"white",
        width:'100%',
        height:'60px',
        marginBottom:'8px',
        display:'flex',
        flexDirection:"row",
        alignItems:'center'
    },
    headerContent:{
        border: '0px solid #FFE7BD',
        borderRadius: '4px',
        backgroundColor:"#FFE7BD",
        width:'100%',
        height:'80px',
        marginBottom:'8px',
        display:'flex',
        flexDirection:"row",
        alignItems:'center'

    },
    itmImg:{
        width:'60px',
        height:'100%'
    },
    headImg:{
        width:'80px',
        height:'100%'
    }
}))

const Card = (props)=>{
    const classes = styles();
 
    return(
        <Fragment>
            <Box className={classes.container}>
                <Box className={classes.header}>
                    <Typography style={{fontSize:'20px', color:'#4D4F5C'}}>{props.title}</Typography>
                    <img src={MoreLogo} className={classes.logo}></img>
                  
                </Box>
                
                
                <Box className={classes.headerContent} >
                    
                    <img src={props.content.head.img} className={classes.headImg}></img>
                    <Box style={{flexDirection:'column', display:'flex', marginLeft:'8px', width:'100%'}}>
                        <Typography style={{color:'#000000DE', fontSize:'20px',marginBottom:'4px'}}>{props.content.head.title}</Typography>
                        <Box style={{display:'flex', flexDirection:'row', justifyContent:"space-between"}}>
                            <Typography style={{fontSize:'14px', color:'#00000099'}}>{props.content.head.price}</Typography>
                            <Typography style={{fontSize:'14px', color:'#00000099', paddingRight:'24px'}}>{props.content.head.terjual}</Typography>
                        </Box>
                    </Box>
                
                </Box>

            
                {
                    props.content.body.map((item,idx)=>{
                        return(
                            <Box className={classes.content} key={idx}>
                                
                                <img src={item.img} className={classes.itmImg}></img>
                                <Box style={{flexDirection:'column', display:'flex', marginLeft:'8px', width:'100%'}}>
                                    <Typography style={{color:'#000000DE', fontSize:'16px'}}>{item.title}</Typography>
                                    <Box style={{display:'flex', flexDirection:'row', justifyContent:"space-between"}}>
                                        <Typography style={{fontSize:'12px', color:'#00000099'}}>{item.price}</Typography>
                                        <Typography style={{fontSize:'12px', color:'#00000099', paddingRight:'24px'}}>{item.terjual}</Typography>
                                    </Box>
                                </Box>
                            
                            </Box>
                        )
                    })
                }

            </Box>
        </Fragment>
    )
}
export default Card