import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Logo from '../assets/advotics.png';
import Dashboard from '../assets/dashboard.svg';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';

import AppBarFixed from './AppBar'

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor:'white'
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor:'white'
 
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
   
    
    backgroundColor:'white'

  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  navLogo:{
    width:'50px',
    height:'auto'
  },
  parent: {
    backgroundColor: 'white'   ,
    '&:hover': {
      backgroundColor: '#D2D2D2'
    }

  },
  menu:{
    height:'21px',
    width:"24px",
    display:'flex',
    flexDirection:'column',
    justifyContent:"space-evenly"
  },
  menuLine:{
    width:"100%",
    height:'2px',
    backgroundColor:'#C5C5C5'
  },
  menuLineHalf:{
    width:"50%",
    height:'2px',
    backgroundColor:'#C5C5C5'
  }
}));

const DefaultLayout = ({children})=> {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
   
      <CssBaseline />
      <AppBarFixed open ={open} handleClick={handleDrawerOpen}/>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
     
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
      
        <List>
          {['Menu','Dashboard'].map((text, index) => 
             
            {
                const getIcon = (text)=>{
                  
                    
                        if (text==='Menu'){
                            return(<div className={classes.menu} onClick={handleDrawerOpen}>
                                <div className={classes.menuLine}></div>
                                <div className={classes.menuLine}></div>
                                <div className={classes.menuLineHalf}></div>
                            </div>)
                        }
                        else if(text==='Dashboard'){
                            return(<img src={Dashboard} style={{width:"24px", height:'21px'}}/>)
                        }
                       
                     
                        
                } 
                return(
            
                    <ListItem button key={text} 
                      className={
                        classes.parent
                      }
                    >
                        <ListItemIcon >{getIcon(text)}</ListItemIcon>
                        <ListItemText primary={text} style={{color:theme.palette.secondary.main}} />
                    </ListItem>
                )}
          )}
        </List>
      
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
            
      </main>
   
    </div>
  );
}
export default DefaultLayout;