import React, {Fragment} from 'react';
import {makeStyles, Box, Typography, IconButton} from '@material-ui/core';
import MoreLogo from '../assets/more.svg'
import CartLogo from '../assets/cart.svg';
import DownArrow from '../assets/DownArrow.svg';

const styles = makeStyles(theme=>({
    container:{
        width:'276px',
        height:'104px',
        backgroundColor:'white',
        boxShadow: '0px 2px 6px #0000000A',
        border: '0.5px solid #CACED5',
        marginTop:"16px",
        marginBottom:'16px'
    },
    header:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        padding:'8px 16px 8px 16px'
    },
    logo:{
        width:"12px",
        height:"auto",
        '&:hover':{
            cursor:"pointer"
        }
    }
}))

const SalesTurnover = ()=>{
    const classes = styles();
    return(
        <Fragment>
            <Box className={classes.container}>
                <Box className={classes.header}>
                    <Typography style={{fontSize:'15px', color:'#43425D', fontWeight:'lighter'}}>Sales Turnover</Typography>
                    <img src={MoreLogo} className={classes.logo}></img>
                </Box>
                <Box style={{display:"flex", flexDirection:'row', padding:"0px 16px 0px 16px"}}>
                    <Box style={{display:'flex', flexDirection:'column'}}>
                        <Typography style={{fontWeight:"bold", color:'#4D4F5C', textTransform:'capitalize',fontSize:'25px'}}>Rp 3,600,000</Typography>
                        <Box style={{display:"flex", flexDirection:"row", alignItems:'center'}}>
                            <img src={DownArrow} style={{width:"8px"}}></img>
                            <Typography style={{fontSize:'10px', color:"#575757"}}><span style={{color:"red"}}>13.8%</span> last preiod on products sold</Typography>
                        </Box>
                    </Box>
                    <img src={CartLogo} style={{width:'48px', height:'auto', marginLeft:"32px"}}></img>
                </Box>
            </Box>

        </Fragment>
    )
}
export default SalesTurnover;