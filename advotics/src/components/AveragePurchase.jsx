import React,{Fragment, useEffect,useRef,useState} from 'react';
import {makeStyles, Box, Typography, IconButton} from '@material-ui/core';
import Chartjs from 'chart.js';
import MoreLogo from '../assets/more.svg';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const chartConfig = {
    type: 'bar',
    data: {
        datasets: [{
            label: 'Net',
            data: [25, 20,18, 20,18,20,22],
            backgroundColor:'#37B04C',
            // this dataset is drawn below
            order: 2
        }, {
            label: 'Gross',
            data: [27, 22,20, 22,20,22,24],
            backgroundColor:'#289E45',
            // this dataset is drawn below
            order: 3
        },
        {
            label: 'Average Purchase Value',
            data: [27, 22,20, 22,20,22,24],
            backgroundColor:'#7AE28C',
            // this dataset is drawn below
            order: 4
        },
        {
            label: 'Unit per Transaction',
            data: [27, 22,20, 22,20,22,24],
            backgroundColor:'#707070',
            // this dataset is drawn below
            order: 5
        }
        ,{
            label: 'Average',
            data: [25, 20, 18, 20,18,20,22],
            type: 'line',
            borderColor: "yellow",
       
            backgroundColor: "yellow",
            pointBackgroundColor: "yellow",
            pointBorderColor: "yellow",
            pointHoverBackgroundColor: "yellow",
            pointHoverBorderColor: "yellow",           
             backgroundColor:'transparent',
            // this dataset is drawn on top
            order: 1
        }],
        labels: ['Mon', 'Tue', 'Wed', 'Thu','Fri','Sat','Sun']
    },
    options: {
        scales:{
            xAxes: [{ stacked: true }],
            
          },
        legend:{
            position:'bottom',
            labels:{
                boxWidth:20
            }
           
        }
    }
  };
  
const styles = makeStyles(theme=>({
    container:{
        width:'570px',
        height:'auto',
        backgroundColor:'white',
        boxShadow: '0px 2px 6px #0000000A',
        border: '0.5px solid #CACED5',
        paddingBottom:'8px'
       
    },
    header:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        padding:'8px 16px 8px 16px',
        alignItems:'center'
    },
    logo:{
        width:"12px",
        height:"auto",
        '&:hover':{
            cursor:"pointer"
        }
    },
    filter:{
        backgroundColor:'white',
        boxShadow: '0px 2px 3px #0000000D',
        border: '1px solid #D7DAE2',
        borderRadius: '2px',
        display:'flex',
        flexDirection:'row',
        alignItems:'center',
        paddingLeft:'8px',
        marginRight:'8px',
        maxHeight:'32px'
    }
}))

const AveragePurchase = ()=>{
    const chartContainer = useRef(null);
    const [chartInstance, setChartInstance] = useState(null);
    const classes = styles()
    useEffect(() => {
        if (chartContainer && chartContainer.current) {
          const newChartInstance = new Chartjs(chartContainer.current, chartConfig);
          setChartInstance(newChartInstance);
        }
      }, [chartContainer]);
    const updateDataset = (datasetIndex, newData) => {
        chartInstance.data.datasets[datasetIndex].data = newData;
        chartInstance.update();
      };
    
    const onButtonClick = () => {
        const data = [1, 2, 3, 4, 5, 6];
        updateDataset(0, data);
      };
    return(
        <Fragment>
            <Box className={classes.container}>
                <Box className={classes.header}>
                    <Typography style={{fontSize:'20px', color:'#4D4F5C'}}>AVERAGE PURCHASE VALUE</Typography>
                    <Box style={{display:'flex', flexDirection:'row',alignItems:'center'}}>
                        <Box className={classes.filter}>
                            <Typography style={{color:'#4D4F5C', fontSize:'12px'}}>Last 6 Months</Typography>
                           
                            <ExpandMoreIcon style={{marginLeft:'12px', color:'#4D4F5C'}}/>
                        
                        </Box>
                        <img src={MoreLogo} className={classes.logo}></img>
                    </Box>
                   
                </Box>
                <canvas ref={chartContainer} />
            </Box>
        </Fragment>
    )
}
export default AveragePurchase