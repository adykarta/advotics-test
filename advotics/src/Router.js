import React, { Fragment } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import DefaultLayout from './components/DefaultLayout';
import Dashboard from './pages/Dashboard';
import Auth from './Auth';

const Router = (childProps) => {
    return (
        <Fragment>
            <Switch>
                <PrivateRoute exact path='/' component={Dashboard} props={childProps} />
           
            </Switch>
        </Fragment>
    )
}




const PublicRoute = ({ component: Component, props: cProps, ...rest }) => (
	<Route {...rest} render={
		props => !Auth.getAuth() ? (
			<Component {...props} {...cProps} />
		) : (
				<Redirect to={{ pathname: "/home" }} />
			)
	}
	/>
)


const PrivateRoute = ({ component: Component, props: cProps, ...rest }) => (
	<Route {...rest} render={
		props => Auth.getAuth() ? (
			<DefaultLayout {...Auth}>
				<Component {...props} {...cProps} />
			</DefaultLayout>
		) : (
				<Redirect to={{ pathname: "/" }} />
			)
	}
	/>
)


export default Router;